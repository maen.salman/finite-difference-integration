# Finite difference integration

This project concerns a study on the finite difference method that is applied on approximating integrals that need to be numerically evaluated on a grid. Three different programs deal with the:
- Centered
- Forward
- Backward
finite difference approximations.

These programs generate the wanted integration weights, and at the end, the user can integrate a function using derived expressions, and the results are compared with the exact ones.
